<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $fillable = [
        'name',
        'description',
        'image_path',
        'price',
        'quantity',
        'is_avail',
    ];
}
